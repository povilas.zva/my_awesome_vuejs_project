// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)

Vue.config.productionTip = false

let app;
let config = {
  apiKey: "AIzaSyBs8EYwqRozC7e-bU3svUxq-G4wOBZ21fs",
  authDomain: "unitask-604a4.firebaseapp.com",
  databaseURL: "https://unitask-604a4.firebaseio.com",
  projectId: "unitask-604a4",
  storageBucket: "unitask-604a4.appspot.com",
  messagingSenderId: "760537172504"
};

firebase.initializeApp(config)
firebase.auth().onAuthStateChanged(function(user) {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      el: '#app',
      template: '<App/>',
      components: { App },
      router
    })
  }
});
